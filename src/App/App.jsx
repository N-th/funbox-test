import React from 'react'
import ProductCards from '../components/ProductCards/ProductCards'
import './App.scss'

class App extends React.Component {
  state = {
    products: null,
  }
  render() {
    const { products } = this.state
    return (
      <div className="app">
        <div className="bg-wrapper">
          <p className="title">Ты сегодня покормил кота?</p>
          <ProductCards products={products}/>
        </div>
      </div>
    )
  }
}

export default App
