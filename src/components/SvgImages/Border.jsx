import React from "react"
import PropTypes from "prop-types"

class Border extends React.PureComponent {
  render() {
    const { className } = this.props
    return (
      <React.Fragment>
        <svg
          className={className}
          width="100%"
          height="100%"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 320 480"
          >
        <path
          d="M58,2 h250 a10,10 0 0 1 10,10 v456 a10,10 0 0 1 -10,10 h-296 a10,10 0 0 1 -10,-10 v-410 z"
        />
        </svg>
      </React.Fragment>
    )
  }
}

Border.propTypes = {
  className: PropTypes.string.isRequired,
}

export default Border
