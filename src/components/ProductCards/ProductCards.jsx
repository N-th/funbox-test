import React from 'react'
import { products } from '../../App/products'
import ProductItem from '../ProductItem/ProductItem'
import './productCards.scss'

class ProductCards extends React.Component {
  renderProducts = () => {
    const productData = products.map(productItem => {
      return <ProductItem key={productItem.id} productItem={productItem}/>
    })
    return productData
  }

  render() {
    return (
      <div className='product-cards'>
        {this.renderProducts()}
      </div>
    )
  }
}

export default ProductCards
