import React from 'react'
import PropTypes from 'prop-types'
import Border from "../SvgImages/Border"
import ItemWeight from '../ItemWeight/ItemWeight'
import './productItem.scss'

class ProductItem extends React.Component {
  state = {
    select: false,
  }

  selected = e => {
    e.preventDefault();
    this.setState(prevState =>({ select: !prevState.select }))
  }

  render() {
    const {
      title,
      name,
      consist,
      portion,
      bonus,
      weight,
      description,
      disabled,
    } = this.props.productItem
    const { select } = this.state
    return (
      <div className="product-item" onClick={this.selected}>
        <div className="product-item__article article">
          {
            disabled ?
              <Border className="article__border article__border--disabled" />
              :
              <Border className={select ? 'article__border article__border--onClick' : 'article__border'} />
          }
          <div className="article__top-wrapper text">
            <p className="text__top">{title}</p>
            <h3 className="text__header">{name}</h3>
            <p className="text__header-after">{consist}</p>
            <p className="text__bottom">
                {portion} {bonus}
            </p>
          </div>
          {
            disabled ?
              <ItemWeight className="article__bottom-wrapper article__bottom-wrapper--disabled" weight={weight}/>
              :
              <ItemWeight className={select ? "article__bottom-wrapper article__bottom-wrapper--onClick" : 'article__bottom-wrapper'} weight={weight}/>
          }
        </div>
        <div className="product-item__description">
          {
            disabled ?
              <p className="product-item__description--disabled">Печалька, {consist} закончился.</p>
              :
                select ?
                  <p>{description}</p>
                  :
                  <p className="">Чего сидишь? Порадуй котэ, <button className="product-item__link">купи.</button></p>
          }
        </div>
      </div>
    )
  }
}

ProductItem.propTypes = {
  productItem: PropTypes.shape({
    title: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    consist: PropTypes.string.isRequired,
    portion: PropTypes.string.isRequired,
    bonus: PropTypes.string.isRequired,
    weight: PropTypes.number.isRequired,
    description: PropTypes.string.isRequired,
    disabled: PropTypes.bool.isRequired,
  }).isRequired,
}

export default ProductItem
