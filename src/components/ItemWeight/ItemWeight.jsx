import React from "react"
import PropTypes from "prop-types"
import './itemWeigth.scss'

class ItemWeight extends React.PureComponent {
  render() {
    const {
      className,
      weight,
    } = this.props
    return (
      <div className={className}>
        <div className="bottom-text">
          <p className="bottom-text__number">{weight}</p>
          <p className="bottom-text__text">кг</p>
        </div>
      </div>
    )
  }
}

ItemWeight.propTypes = {
    className: PropTypes.string.isRequired,
    weight: PropTypes.number.isRequired,
}

export default ItemWeight
